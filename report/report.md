# Testing of SimplCommerce application

SimplCommerce is an opensource application that is used as a skeleton for actual web e-shops. It is available at https://github.com/simplcommerce/SimplCommerce with the deployed version of the example shop at: https://ci.simplcommerce.com/ I tested the example application. Due to the large scope of the application, I reduced the domain to a few specific tasks that are most likely to be used by normal users.

## Boundary Values and EC analysis
I selected 3 different forms in the application and analyzed their EC and boundary values. Note that there are not many forms that would be accessible to a normal user in the application. Specifically I found only these:

### Cart form
Each of the items in the cart has a quantity variable and the whole cart has coupon variable.

**Quantity** - I have split this in 2 EC. As this option is controller by +- buttons it is not possible to enter non-integers and values `<1`.
* `1` 
* `>=2`

So I used these two values as boundary values.

**Coupon** - There are 2 EC:
* existing coupon code
* non-existent coupon code

Boundary values for these EC could be: `valid coupon code`, `string with hamming distance of 1 from some valid coupon code`.

**Description** - arbitrary string. Possibilities could be `empty` and `non-empty` however these belong to the same EC as there should be no difference between them whatsoever.


### Login form
There are 3 variables in this form. 

**Email** - There are 2 EC:
* `not email format` - invalid technical
* `not registered email` - invalid business
* `registered email` - valid

If we have a registered email `stuff@stuff.stuff` then boundary values for these EC could be: `stuff`, `stuff.stuff@stuff.stuff`, `stuff@stuff.stuff`.

**Password** - There are just trivial 2 EC:
* `valid`
* `invalid`

For boundary values we could use a `valid password` and `string with hamming distance of 1 from valid password`.


**Remember** - boolean: boundary values: `true`/ `false`

### Register form 
There are 3 checked variables in this form.

**Email** - There are 3 EC:
* `not email format` - invalid technical
* `registered email` - invalid business
* `not registered email` - valid

If we have a registered email `stuff@stuff.stuff` then boundary values for these EC could be `stuff`, `stuff@stuff.stuff`, `stuffs@stuff.stuff`.

**Password** - There are 3 EC:
* `len < 4` - invalid business
* `len >= 4 && len <= 100` - valid
* `len > 100` - invalid business

As there is no requirement for the characters and the password can contain any characters, boundary values could be as such: `!!!`, `aaa!`, `'b' * 100`, `'b' * 101`.

**Confirmation** - There are 2 trivial EC:
* `matching` - valid 
* `not matching` - invalid business
