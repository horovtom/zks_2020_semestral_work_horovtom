package cz.cvut.horovtom.zks.semestral.pom.pages;

import cz.cvut.horovtom.zks.semestral.PropertiesContainer;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends BasePage {
    @FindBy(id = "Email")
    WebElement emailField;

    @FindBy(id = "Password")
    WebElement passwordField;

    @FindBy(id = "RememberMe")
    WebElement rememberField;

    @FindBy(xpath = "/html/body/div[4]/div/div[1]/section/form/div[5]/div/button")
    WebElement loginButton;

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    @Override
    protected String getPageUrl() {
        return PropertiesContainer.getBaseUrl() + "/login";
    }

    public void setEmail(String email) {
        emailField.clear();
        emailField.sendKeys(email);
    }

    public void setPassword(String password) {
        passwordField.clear();
        passwordField.sendKeys(password);
    }

    public void setRemember(boolean value) {
        if (rememberField.isSelected() != value)
            rememberField.click();
    }

    public void clickLogin() {
        loginButton.click();
    }

    public void login(String email, String password, boolean remember) {
        setEmail(email);
        setPassword(password);
        setRemember(remember);
        clickLogin();
    }

    public void login(String email, String password)  {
        login(email, password, false);
    }
}
