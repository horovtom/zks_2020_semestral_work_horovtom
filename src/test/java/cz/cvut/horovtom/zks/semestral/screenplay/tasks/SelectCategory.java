package cz.cvut.horovtom.zks.semestral.screenplay.tasks;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.thucydides.core.annotations.Step;

import static cz.cvut.horovtom.zks.semestral.screenplay.pageobjects.ApplicationHomePage.CATEGORY_NAME;


public class SelectCategory  implements  Task{
    String categoryName;

    public SelectCategory(String categoryName) {
        this.categoryName = categoryName;
    }

    public static SelectCategory called(String categoryName) {
        return Instrumented.instanceOf(SelectCategory.class).withProperties(categoryName);
    }

    @Step("{0} selects category called #categoryName")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(CATEGORY_NAME.of(categoryName)));
    }
}
