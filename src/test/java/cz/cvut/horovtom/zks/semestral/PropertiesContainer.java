package cz.cvut.horovtom.zks.semestral;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesContainer {
    private static Properties properties = null;

    private PropertiesContainer() {
    }


    private static void loadProperties() {
        InputStream inputStream = null;
        try {
            properties = new Properties();
            String propFileName = "config.properties";

            inputStream = PropertiesContainer.class.getClassLoader().getResourceAsStream(propFileName);

            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new Exception("property file '" + propFileName + "' not found in the classpath");
            }
        } catch (Exception e) {
            System.out.println("Exception: " + e);
            assert false;
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    assert false;
                }
            }
        }
    }

    private static String getValue(String key) {
        if (properties == null)
            loadProperties();

        return properties.getProperty(key);
    }

    public static String getBaseUrl() {
        return getValue("baseUrl");
    }

    public static String getAdminEmail() {
        return getValue("adminEmail");
    }

    public static String getAdminPassword( ) {
        return getValue("adminPassword");
    }

    public static String getCapablancaName() {
        return getValue("capablancaName");
    }

    public static String getCapablancaEmail() {
        return getValue("capablancaEmail");
    }

    public static String getCapablancaPassword() {
        return getValue("capablancaPassword");
    }

    public static String getStuffName() {
        return getValue("stuffName");
    }

    public static String getStuffEmail() {
        return getValue("stuffEmail");
    }

    public static String getStuffPassword() {
        return getValue("stuffPassword");
    }

    public static String getActionName() {
        return getValue("actionName");
    }
    public static String getActionAmount() {
        return getValue("actionAmount");
    }
    public static String getActionCode() {
        return getValue("actionCode");
    }
}
