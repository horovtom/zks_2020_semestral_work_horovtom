package cz.cvut.horovtom.zks.semestral.pom.pages;

import cz.cvut.horovtom.zks.semestral.PropertiesContainer;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class CartRules extends BasePage {
    @FindBy(linkText = "Create Cart Price Rule")
    WebElement createRuleButton;

    @FindBy(name="name")
    WebElement actionNameField;

    @FindBy(name="couponCode")
    WebElement couponCodeField;

    @FindBy(xpath="//label[text() = 'Apply *']/..//select")
    WebElement applyField;

    @FindBy(name="discountAmount")
    WebElement discountAmountField;

    @FindBy(xpath = "/html/body/div[2]/div/div/div[2]/form/div[2]/div/div[1]/div[13]/div/div/label/input")
    WebElement isActiveCheck;

    @FindBy(xpath = "//div[@class='form-group']//button[contains(@class, 'btn-primary')]")
    WebElement saveButton;

    public CartRules(WebDriver driver) {
        super(driver);
    }

    public static CartRules goTo(WebDriver driver) {
        String url = PropertiesContainer.getBaseUrl() + "/admin#!/cart-rules";
        driver.get(url);

        return new CartRules(driver);
    }

    @Override
    protected String getPageUrl() {
        return PropertiesContainer.getBaseUrl() + "/admin#!/cart-rules";
    }

    private void selectFixedAmount() {
        Select applySelect = new Select(applyField);
        applySelect.selectByIndex(1);
    }

    private boolean hasAction(String name) throws InterruptedException {
        Thread.sleep(1000);
        List<WebElement> elements = driver.findElements(By.xpath("//tbody/tr/td[@class='ng-binding']"));
        for (WebElement element : elements) {
            if (element.getText().equals(name))
                return true;
        }
        return false;
    }

    public void createAction(String name, String code, String amount) throws InterruptedException {
        if (hasAction(name))
            return;
        createRuleButton.click();
        Thread.sleep(150);
        setActionName(name);
        setCode(code);
        selectFixedAmount();
        setAmount(amount);
        if (!isActiveCheck.isSelected())
            isActiveCheck.click();
        Thread.sleep(100);
        saveButton.click();
        Thread.sleep(1000);
    }

    private void setAmount(String amount) {
        discountAmountField.clear();
        discountAmountField.sendKeys(amount);
    }

    private void setCode(String code) {
        couponCodeField.clear();
        couponCodeField.sendKeys(code);
    }

    private void setActionName(String name) {
        actionNameField.clear();
        actionNameField.sendKeys(name);
    }


}
