package cz.cvut.horovtom.zks.semestral.screenplay.pageobjects;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://localhost:5000/esprit-ruffle-shirt")
public class ItemDetailPage extends PageObject {
    public static final Target PRICE = Target
            .the("Price")
            .locatedBy("//div[@class=\"product-price\"]/h3");
}
