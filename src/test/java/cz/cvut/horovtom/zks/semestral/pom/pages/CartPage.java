package cz.cvut.horovtom.zks.semestral.pom.pages;

import cz.cvut.horovtom.zks.semestral.PropertiesContainer;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class CartPage extends BasePage {

    @FindBy(xpath = "//a[text() = \"Process to Checkout\"]")
    WebElement processToCheckoutButton;

    @FindBy(name = "couponCode")
    WebElement couponCodeField;

    @FindBy(xpath = "//div[@class=\"form-group\"]/button[text() = \"Apply\"]")
    WebElement applyCodeField;

    @FindBy(name = "orderNote")
    WebElement orderNoteField;

    @FindBy(xpath = "//div[contains(@class, \"order-summary\")]//td[contains(text(), \"Discount\")]/../td[2]")
    WebElement discountText;

    @FindBy(xpath = "//div[contains(@class, \"order-summary\")]//td[contains(text(), \"Subtotal\")]/../td[2]")
    WebElement subtotalText;

    @FindBy(xpath = "//div[contains(@class, \"order-summary\")]//td[contains(text(), \"Order Total\")]/../td[2]")
    WebElement totalText;

    @FindBy(xpath = "//button[text() = \"Save order note\"]")
    WebElement saveNoteButton;

    protected CartPage(WebDriver driver) {
        super(driver);
    }

    @Override
    protected String getPageUrl() {
        return PropertiesContainer.getBaseUrl() + "/cart";
    }

    public static CartPage goTo(WebDriver driver) {
        driver.get(PropertiesContainer.getBaseUrl() + "/cart");
        return new CartPage(driver);
    }

    public void clearCart() throws InterruptedException {
        JavascriptExecutor executor = (JavascriptExecutor) driver;

        driver.navigate().refresh();
        Thread.sleep(1000);
        List<WebElement> elements = driver.findElements(By.xpath("//button/span[@class=\"fa fa-remove\"]/.."));

        while(!elements.isEmpty()) {
            executor.executeScript("arguments[0].click();", elements.get(0));
            Thread.sleep(100);
            driver.navigate().refresh();
            Thread.sleep(1000);
            elements = driver.findElements(By.xpath("//button/span[@class=\"fa fa-remove\"]/.."));
        }
    }

    // Disgusting...
    private void retryClick(By by) {
        int attempts = 0;
        while (attempts < 10) {
            try {
                driver.findElement(by).click();
                return;
            } catch (StaleElementReferenceException ignored) {
            }
            attempts++;
        }
    }

    public int getQuantity() {
        int curr = 1;
        int attempts = 0;
        while (attempts < 10) {
            try {
                curr = Integer.parseInt(driver.findElement(By.xpath("//td[@class=\"text-center\"]/input[contains(@class, \"quantity-field\")]")).getAttribute("value"));
            } catch (StaleElementReferenceException ignored) {
            }
            attempts++;
        }
        return curr;
    }

    public void setQuantity(int amount) throws InterruptedException {
        while (true) {
            int curr = getQuantity();
            if (amount > curr)
                retryClick(By.xpath("//button[@class=\"quantity-button\" and @value=\"+\"]"));
            else if (amount < curr)
                retryClick(By.xpath("//button[@class=\"quantity-button\" and @value=\"-\"]"));
            else
                break;
        }
    }

    public void setCouponCode(String code) {
        couponCodeField.clear();
        couponCodeField.sendKeys(code);
    }

    public void applyCouponCode() {
        applyCodeField.click();
    }

    public boolean isCodeApplied() {
        return getDiscount() > 0;
    }

    public double getDiscount() {
        return getFloatVar(discountText);
    }

    public void setNote(String note) {
        orderNoteField.clear();
        orderNoteField.sendKeys(note);
        saveNoteButton.click();
    }

    public boolean canProcessToCheckout() {
        return processToCheckoutButton.isEnabled();
    }

    public void setDetails(int quantity, String coupon, String description) throws InterruptedException {
        setCouponCode(coupon);
        applyCouponCode();
        setQuantity(quantity);
        setNote(description);
    }

    private double getFloatVar(WebElement var) {
        String text = var.getText();
        assertTrue(text.startsWith("$"));
        text = text.replace("$", "");
        text = text.replace(",", "");
        return Double.parseDouble(text);
    }

    public double getSubtotal() {
        return getFloatVar(subtotalText);
    }

    public double getTotal() {
        return getFloatVar(totalText);
    }
}
