package cz.cvut.horovtom.zks.semestral.screenplay.questions;

import cz.cvut.horovtom.zks.semestral.screenplay.pageobjects.ItemDetailPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.matchers.WebElementStateMatchers;
import net.serenitybdd.screenplay.questions.Text;
import net.serenitybdd.screenplay.questions.WebElementQuestion;
import net.serenitybdd.screenplay.waits.Wait;

public class Price implements Question<Double> {

    public static Question<Double> forCurrentItem() {
        return new Price();
    }

    @Override
    public Double answeredBy(Actor actor){
        actor.attemptsTo(Wait.until(WebElementQuestion.the(ItemDetailPage.PRICE), WebElementStateMatchers.isEnabled()).forNoLongerThan(5).seconds());
        String res = Text.of(ItemDetailPage.PRICE).viewedBy(actor).resolve().replace("$", "");
        return Double.parseDouble(res);
    }
}
