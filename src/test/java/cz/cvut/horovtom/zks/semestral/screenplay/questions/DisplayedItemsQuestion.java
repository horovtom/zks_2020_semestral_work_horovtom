package cz.cvut.horovtom.zks.semestral.screenplay.questions;

import cz.cvut.horovtom.zks.semestral.screenplay.pageobjects.ApplicationHomePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.CountQuestion;
import net.serenitybdd.screenplay.questions.Text;

import java.util.List;

public class DisplayedItemsQuestion implements Question<List<String>> {

    public static Question<Integer> displayedItemsCount() {
        return new CountQuestion(new DisplayedItemsQuestion());
    }

    public static Question<List<String>> displayedItems() {
        return new DisplayedItemsQuestion();
    }

    @Override
    public List<String> answeredBy(Actor actor){
        return Text.of(ApplicationHomePage.ITEMS).viewedBy(actor).asList();
    }
}
