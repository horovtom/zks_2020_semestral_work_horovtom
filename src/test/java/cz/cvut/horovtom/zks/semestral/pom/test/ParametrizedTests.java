package cz.cvut.horovtom.zks.semestral.pom.test;

import cz.cvut.fel.still.sqa.seleniumStarterPack.config.DriverFactory;
import cz.cvut.horovtom.zks.semestral.pom.pages.CartPage;
import cz.cvut.horovtom.zks.semestral.pom.pages.HomePage;
import cz.cvut.horovtom.zks.semestral.pom.pages.ProductDetailPage;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.openqa.selenium.WebDriver;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ParametrizedTests {
    WebDriver driver;
    TestUtil util;
    private static boolean createdCoupon = false;

    private static WebDriver createDriver() throws IOException {
        WebDriver driver = new DriverFactory().getDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(200, TimeUnit.MILLISECONDS);
        driver.manage().deleteAllCookies();
        return driver;
    }

    @BeforeEach
    public void before() throws IOException {
        driver = createDriver();
        util = new TestUtil(driver);
    }

    @AfterEach
    public void after() throws IOException {
        driver.close();
        util = null;
    }

    @ParameterizedTest
    @CsvFileSource(resources = {"/Login-output-2way.csv", "/Login-output-3way.csv", "/Login-output-mixed.csv"}, numLinesToSkip = 1)
    public void testLoginParametrised(String email, String password, boolean remember, boolean result) {
        System.out.println("Testing email:" + email + ", password:" + password + ", remember:" + remember + ", result:" + result);

        HomePage homePage = HomePage.goToUnlogged(driver);
        util.ensureStuffIsRegistered(homePage);

        homePage.logout();
        assertEquals(result, util.checkLoginSuccess(homePage, email, password, remember));
    }

    private CartPage getUserWithProdInCart() throws InterruptedException {
        HomePage homePage = HomePage.goToUnlogged(driver);
        util.ensureAndLoginStuff(homePage);
        util.clearCart();

        homePage = HomePage.goTo(driver);
        homePage.clickOnProduct(0);
        ProductDetailPage productDetailPage = new ProductDetailPage(driver);
        productDetailPage.addToCart();

        CartPage cartPage = CartPage.goTo(driver);
        Thread.sleep(500);
        return cartPage;
    }

    @ParameterizedTest
    @CsvFileSource(resources = {"/Cart-output-2way.csv", "/Cart-output-3way.csv", "/Cart-output-mixed.csv"}, numLinesToSkip = 1)
    public void testCartCoupon(String coupon, String description, int quantity, boolean couponExists) throws InterruptedException {
        System.out.println("Testing quantity:" + quantity + " coupon:" + coupon + " description:" + description + " couponExists:" + couponExists);
        if (!createdCoupon)
            util.createCoupon();
        createdCoupon = true;

        CartPage cartPage = getUserWithProdInCart();
        cartPage.setDetails(quantity, coupon, description);

        assertEquals(couponExists, cartPage.isCodeApplied());
        cartPage.clearCart();
    }

    @ParameterizedTest
    @CsvFileSource(resources = {"/Cart-output-2way.csv", "/Cart-output-3way.csv", "/Cart-output-mixed.csv"}, numLinesToSkip = 1)
    public void testCartPriceCounting(String coupon, String description, int quantity, boolean couponExists) throws InterruptedException {
        System.out.println("Testing quantity:" + quantity + " coupon:" + coupon + " description:" + description + " couponExists:" + couponExists);

        CartPage cartPage = getUserWithProdInCart();
        cartPage.setQuantity(1);
        double subtotal = cartPage.getSubtotal();
        double discount = cartPage.getDiscount();
        double orderTotal = cartPage.getTotal();

        assertEquals(0.0, discount);
        assertEquals(subtotal, orderTotal);

        cartPage.setDetails(quantity, coupon, description);

        double newSubtotal = cartPage.getSubtotal();
        discount = cartPage.getDiscount();
        orderTotal = cartPage.getTotal();

        assertEquals(subtotal * quantity, newSubtotal);
        assertEquals(couponExists, discount > 0.0);
        assertEquals(orderTotal, quantity * subtotal - discount);
        cartPage.clearCart();
    }
}
