package cz.cvut.horovtom.zks.semestral.screenplay.tasks;

import cz.cvut.horovtom.zks.semestral.screenplay.pageobjects.ItemDetailPage;
import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.questions.Visibility;
import net.serenitybdd.screenplay.waits.Wait;
import net.thucydides.core.annotations.Step;

import static cz.cvut.horovtom.zks.semestral.screenplay.pageobjects.ApplicationHomePage.PRODUCT_NAME;
import static org.hamcrest.Matchers.is;


public class SelectProduct implements Task {
    String productName;

    public SelectProduct(String productName) {
        this.productName = productName;
    }

    public static SelectProduct named(String categoryName) {
        return Instrumented.instanceOf(SelectProduct.class).withProperties(categoryName);
    }

    @Step("{0} selects product called #productName")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Wait.until(Visibility.of(PRODUCT_NAME.of(productName)).viewedBy(actor).asAQuestion(), is(true)).forNoLongerThan(2).seconds());
        actor.attemptsTo(
                Click.on(PRODUCT_NAME.of(productName)).then(
                        Wait.until(Visibility.of(ItemDetailPage.PRICE).viewedBy(actor).asAQuestion(), is(true))
                                .forNoLongerThan(2).seconds()));
    }
}
