package cz.cvut.horovtom.zks.semestral.screenplay.pageobjects;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://localhost:5000/login")
public class LoginPage extends PageObject {
    public static final Target email = Target
            .the("Email field")
            .located(By.id("Email"));

    public static final Target password = Target
            .the("Password field")
            .located(By.id("Password"));

    public static final Target loginButton = Target
            .the("Login button")
            .locatedBy("/html/body/div[4]/div/div[1]/section/form/div[5]/div/button");

}
