package cz.cvut.horovtom.zks.semestral.pom.pages;

import cz.cvut.horovtom.zks.semestral.PropertiesContainer;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RegisterPage extends BasePage {
    @FindBy(id = "Email")
    WebElement emailField;

    @FindBy(id = "FullName")
    WebElement nameField;

    @FindBy(id="Password")
    WebElement passwordField;

    @FindBy(id="ConfirmPassword")
    WebElement confirmPasswordField;

    @FindBy(xpath = "/html/body/div[4]/div/div[1]/form/div[6]/div/button")
    WebElement registerButton;

    public RegisterPage(WebDriver driver) {
        super(driver);
    }

    @Override
    protected String getPageUrl() {
        return PropertiesContainer.getBaseUrl() + "/register";
    }

    public void setEmailField(String value) {
        emailField.clear();
        emailField.sendKeys(value);
    }

    public void setNameField(String value) {
        nameField.clear();
        nameField.sendKeys(value);
    }

    public void setPasswordField(String value) {
        passwordField.clear();
        passwordField.sendKeys(value);
    }

    public void setConfirmPasswordField(String value) {
        confirmPasswordField.clear();
        confirmPasswordField.sendKeys(value);
    }

    public void clickRegister() {
        registerButton.click();
    }

    public void register(String email, String fullName, String password) {
        setEmailField(email);
        setNameField(fullName);
        setPasswordField(password);
        setConfirmPasswordField(password);
        clickRegister();
    }
}
