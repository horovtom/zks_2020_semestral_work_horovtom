package cz.cvut.horovtom.zks.semestral.pom.test;

import cz.cvut.horovtom.zks.semestral.PropertiesContainer;
import cz.cvut.horovtom.zks.semestral.pom.pages.*;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.WebDriver;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestUtil {
    public WebDriver driver;

    TestUtil(WebDriver driver) {
        this.driver = driver;
    }

    void ensureCapablancaIsRegistered(HomePage home) {
        ensureUserIsRegistered(home, PropertiesContainer.getCapablancaEmail(), PropertiesContainer.getCapablancaName(), PropertiesContainer.getCapablancaPassword());
    }

    void ensureStuffIsRegistered(HomePage home) {
        ensureUserIsRegistered(home, PropertiesContainer.getStuffEmail(), PropertiesContainer.getStuffName(), PropertiesContainer.getStuffPassword());
    }

    void loginStuff(HomePage home) {
        loginUser(home, PropertiesContainer.getStuffEmail(), PropertiesContainer.getStuffPassword(), false);
    }

    void ensureAndLoginStuff(HomePage home) {
        ensureStuffIsRegistered(home);
        loginStuff(home);
    }

    void clearCart() throws InterruptedException {
        CartPage cartPage = CartPage.goTo(driver);
        cartPage.clearCart();
    }

    void ensureUserIsRegistered(HomePage home, String email, String name, String password) {
        home.logout();
        home.clickRegister();
        RegisterPage registerPage = new RegisterPage(driver);
        assertTrue(registerPage.isOnPage());
        registerPage.register(email, name, password);
        if (home.isOnPage()) {
            assertTrue(home.isLoggedIn());
            assertFalse(home.isAdminLoggedIn());
            home.logout();
        } else {
            home = HomePage.goTo(driver);
            assertFalse(home.isLoggedIn());
        }
    }

    void loginUser(HomePage home, String email, String password, boolean remember) {
        home.logout();
        home.clickLogin();
        LoginPage loginPage = new LoginPage(driver);
        assertTrue(loginPage.isOnPage());
        loginPage.login(email, password, remember);
        if (loginPage.isOnPage())
            HomePage.goTo(driver);
    }

    boolean checkLoginSuccess(HomePage home, String email, String password, boolean remember) {
        loginUser(home, email, password, remember);
        boolean res = home.isLoggedIn();
        home.logout();
        return res;
    }

    public void loginAsAdmin() {
        HomePage home = HomePage.goToUnlogged(driver);

        home.clickLogin();
        LoginPage loginPage = new LoginPage(driver);
        Assertions.assertTrue(loginPage.isOnPage());
        loginPage.login(PropertiesContainer.getAdminEmail(), PropertiesContainer.getAdminPassword());
        Assertions.assertTrue(home.isOnPage());
        Assertions.assertTrue(home.isLoggedIn());
    }

    public void createCoupon() throws InterruptedException {
        System.err.println("Creating coupon");
        loginAsAdmin();
        CartRules cartRules = CartRules.goTo(driver);
        cartRules.createAction(PropertiesContainer.getActionName(), PropertiesContainer.getActionCode() , PropertiesContainer.getActionAmount());
        HomePage.goToUnlogged(driver);
    }
}
