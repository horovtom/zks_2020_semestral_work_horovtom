package cz.cvut.horovtom.zks.semestral.screenplay.abilities;

import net.serenitybdd.screenplay.Ability;
import net.serenitybdd.screenplay.Actor;

import static org.junit.Assert.assertTrue;

public class Authenticate implements Ability {
    private final String email;
    private final String password;

    public static Authenticate with(String email, String password)
    {
        return new Authenticate(email, password);
    }

    public static Authenticate as(Actor actor) {
        if (actor.abilityTo(Authenticate.class) == null)
            assertTrue(false);

        return actor.abilityTo(Authenticate.class);
    }

    public String email() {
        return this.email;
    }

    public String password() {
        return this.password;
    }

    private Authenticate(String email, String password) {
        this.email = email;
        this.password = password;
    }

}
