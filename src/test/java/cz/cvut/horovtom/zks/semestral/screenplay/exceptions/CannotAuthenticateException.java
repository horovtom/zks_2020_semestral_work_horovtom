package cz.cvut.horovtom.zks.semestral.screenplay.exceptions;

public class CannotAuthenticateException extends Throwable {
    public CannotAuthenticateException(String name) {
        super("Actor " + name + " cannot login!");
    }
}
