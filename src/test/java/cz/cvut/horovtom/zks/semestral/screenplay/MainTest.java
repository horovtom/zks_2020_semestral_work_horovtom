package cz.cvut.horovtom.zks.semestral.screenplay;

import cz.cvut.fel.still.sqa.seleniumStarterPack.config.DriverFactory;
import cz.cvut.horovtom.zks.semestral.PropertiesContainer;
import cz.cvut.horovtom.zks.semestral.screenplay.abilities.Authenticate;
import cz.cvut.horovtom.zks.semestral.screenplay.pageobjects.ApplicationHomePage;
import cz.cvut.horovtom.zks.semestral.screenplay.pageobjects.LoginPage;
import cz.cvut.horovtom.zks.semestral.screenplay.questions.DisplayedItemsQuestion;
import cz.cvut.horovtom.zks.semestral.screenplay.questions.Price;
import cz.cvut.horovtom.zks.semestral.screenplay.tasks.Login;
import cz.cvut.horovtom.zks.semestral.screenplay.tasks.SelectCategory;
import cz.cvut.horovtom.zks.semestral.screenplay.tasks.SelectProduct;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Open;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import java.io.IOException;
import java.util.ArrayList;

import static net.serenitybdd.screenplay.GivenWhenThen.*;
import static org.hamcrest.core.Is.is;

@RunWith(SerenityRunner.class)
public class MainTest {

    Actor capablanca =
            Actor.named(PropertiesContainer.getCapablancaName())
                    .whoCan(Authenticate.with(
                            PropertiesContainer.getCapablancaEmail(),
                            PropertiesContainer.getCapablancaPassword()
                    ));

    private WebDriver theBrowser;

    @Before
    public void before() throws IOException {
        theBrowser = new DriverFactory().getDriver();
        theBrowser.manage().window().maximize();
        theBrowser.manage().deleteAllCookies();

        capablanca.can(BrowseTheWeb.with(theBrowser));
    }

    @Test
    public void switchToShoesCategory() {
        String categoryName = "Shoes";

        givenThat(capablanca).wasAbleTo(Open.browserOn().the(ApplicationHomePage.class));
        when(capablanca).attemptsTo(SelectCategory.called(categoryName));
        then(capablanca).should(seeThat(DisplayedItemsQuestion.displayedItemsCount(), is(Matchers.greaterThan(0))));
    }

    @Test
    public void readingWomanCategory() {
        String categoryName = "Woman";
        givenThat(capablanca).wasAbleTo(Open.browserOn().the(ApplicationHomePage.class));
        when(capablanca).attemptsTo(SelectCategory.called(categoryName));
        then(capablanca).should(seeThat(DisplayedItemsQuestion.displayedItemsCount(), is(Matchers.equalTo(10))));
    }

    @Test
    public void productMatches() {
        String categoryName = "Woman";
        givenThat(capablanca).wasAbleTo(
                Open.browserOn().the(LoginPage.class),
                Login.withCredentials());
        when(capablanca).attemptsTo(
                Open.browserOn().the(ApplicationHomePage.class),
                SelectCategory.called(categoryName));
        final ArrayList<String> items = (ArrayList<String>) capablanca.asksFor(DisplayedItemsQuestion.displayedItems());
        when(capablanca).attemptsTo(SelectProduct.named(items.get(0)));
        then(capablanca).should(seeThat(Price.forCurrentItem(), is(Matchers.equalTo(16.64))));
    }

    @After
    public void closeBrowser() {
        theBrowser.close();
    }
}
