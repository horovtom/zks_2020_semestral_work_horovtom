package cz.cvut.horovtom.zks.semestral.screenplay.tasks;

import cz.cvut.horovtom.zks.semestral.screenplay.abilities.Authenticate;
import cz.cvut.horovtom.zks.semestral.screenplay.pageobjects.LoginPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.thucydides.core.annotations.Step;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class Login implements Task {
    public static Login withCredentials() {
        return instrumented(Login.class);
    }

    private Authenticate authenticated(Actor actor) {
        return Authenticate.as(actor);
    }

    @Override
    @Step("Logs in as: {0}")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue(authenticated(actor).email())
                        .into(LoginPage.email),
                Enter.theValue(authenticated(actor).password())
                        .into(LoginPage.password),
                Click.on(LoginPage.loginButton)
        );
    }
}
