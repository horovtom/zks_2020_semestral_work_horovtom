package cz.cvut.horovtom.zks.semestral.pom.test;

import cz.cvut.fel.still.sqa.seleniumStarterPack.config.DriverFactory;
import cz.cvut.horovtom.zks.semestral.pom.pages.CartPage;
import cz.cvut.horovtom.zks.semestral.pom.pages.HomePage;
import cz.cvut.horovtom.zks.semestral.pom.pages.ProductDetailPage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ProductsTest {
    WebDriver driver;
    TestUtil util;

    @Before
    public void before() throws IOException {
        driver = new DriverFactory().getDriver();
        util = new TestUtil(driver);
    }

    @After
    public void after() throws IOException {
        driver.close();
        util = null;
    }

    @Test
    public void multipleItemsInCart() throws InterruptedException {
        HomePage homePage = HomePage.goToUnlogged(driver);
        util.ensureAndLoginStuff(homePage);

        CartPage.goTo(driver).clearCart();
        HomePage.goTo(driver);

        homePage.clickOnProduct(1);
        ProductDetailPage prod = new ProductDetailPage(driver);
        double price = prod.getPrice();
        prod.addToCart();

        homePage = HomePage.goTo(driver);
        homePage.clickOnProduct(2);
        price += prod.getPrice();
        prod.addToCart();

        CartPage cartPage = CartPage.goTo(driver);
        assertEquals(price, cartPage.getSubtotal());
        cartPage.clearCart();
    }

    @Test
    public void higherCountToCart() throws InterruptedException {
        int count = 5;
        HomePage homePage = HomePage.goToUnlogged(driver);
        util.ensureAndLoginStuff(homePage);

        CartPage.goTo(driver).clearCart();
        HomePage.goTo(driver);

        homePage.clickOnProduct(1);
        ProductDetailPage prod = new ProductDetailPage(driver);
        double price = prod.getPrice();
        prod.setQuantity(count);
        prod.addToCart();

        CartPage cartPage = CartPage.goTo(driver);
        assertEquals(count, cartPage.getQuantity());
        assertEquals(count * price, cartPage.getSubtotal());
        assertEquals(count * price, cartPage.getTotal());
        cartPage.clearCart();
    }
}
