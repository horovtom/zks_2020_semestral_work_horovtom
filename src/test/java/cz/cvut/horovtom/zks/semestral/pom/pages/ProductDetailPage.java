package cz.cvut.horovtom.zks.semestral.pom.pages;

import cz.cvut.horovtom.zks.semestral.PropertiesContainer;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ProductDetailPage extends  BasePage {

    @FindBy(xpath = "//button[text()=\"Add to cart\"]")
    WebElement addToCartButton;

    @FindBy(xpath = "//div[@class=\"product-price\"]/h3")
    WebElement priceText;

    public ProductDetailPage(WebDriver driver) {
        super(driver);
    }

    @Override
    protected String getPageUrl() {
        return PropertiesContainer.getBaseUrl() + "/lightweight-jacket";
    }

    public void addToCart() throws InterruptedException {
        Thread.sleep(1500);
        addToCartButton.click();
        Thread.sleep(100);
    }

    public double getPrice() {
        String text = priceText.getText();
        text = text.replace("$", "");
        double val = Double.parseDouble(text);
        return val;
    }

    public int getQuantity() {
        int curr = 1;
        int attempts = 0;
        while (attempts < 10) {
            try {
                curr = Integer.parseInt(driver.findElement(By.xpath("//div[@class=\"product-quantity\"]/input[@class=\"quantity-field\"]")).getAttribute("value"));
            } catch (StaleElementReferenceException ignored) {
            }
            attempts++;
        }
        return curr;
    }

    // Disgusting...
    private void retryClick(By by) {
        int attempts = 0;
        while (attempts < 10) {
            try {
                driver.findElement(by).click();
                return;
            } catch (StaleElementReferenceException ignored) {
            }
            attempts++;
        }
    }

    public void setQuantity(int count) {
        while (true) {
            int curr = getQuantity();
            if (count > curr)
                retryClick(By.xpath("//button[@class=\"quantity-button\" and @value=\"+\"]"));
            else if (count < curr)
                retryClick(By.xpath("//button[@class=\"quantity-button\" and @value=\"-\"]"));
            else
                break;
        }
    }
}
